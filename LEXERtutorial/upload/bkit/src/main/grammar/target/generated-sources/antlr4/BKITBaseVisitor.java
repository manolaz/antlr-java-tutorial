// Generated from BKIT.g4 by ANTLR 4.8

	package main.bkit.parser;

import org.antlr.v4.runtime.tree.AbstractParseTreeVisitor;

/**
 * This class provides an empty implementation of {@link BKITVisitor},
 * which can be extended to create a visitor which only needs to handle a subset
 * of the available methods.
 *
 * @param <T> The return type of the visit operation. Use {@link Void} for
 * operations with no return type.
 */
public class BKITBaseVisitor<T> extends AbstractParseTreeVisitor<T> implements BKITVisitor<T> {
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code ctx}.</p>
	 */
	@Override public T visitProgram(BKITParser.ProgramContext ctx) { return visitChildren(ctx); }
}