// Generated from BKIT.g4 by ANTLR 4.8

	package main.bkit.parser;

import org.antlr.v4.runtime.Lexer;
import org.antlr.v4.runtime.CharStream;
import org.antlr.v4.runtime.Token;
import org.antlr.v4.runtime.TokenStream;
import org.antlr.v4.runtime.*;
import org.antlr.v4.runtime.atn.*;
import org.antlr.v4.runtime.dfa.DFA;
import org.antlr.v4.runtime.misc.*;

@SuppressWarnings({"all", "warnings", "unchecked", "unused", "cast"})
public class BKITLexer extends Lexer {
	static { RuntimeMetaData.checkVersion("4.8", RuntimeMetaData.VERSION); }

	protected static final DFA[] _decisionToDFA;
	protected static final PredictionContextCache _sharedContextCache =
		new PredictionContextCache();
	public static final int
		T__0=1, T__1=2, T__2=3, INT=4, SUBREAL=5, DOT=6, REAL=7, ID=8, WS=9, UNCLOSE_STRING=10, 
		ILLEGAL_ESCAPE=11, UNTERMINATE_COMMENT=12, ERROR_CHAR=13;
	public static String[] channelNames = {
		"DEFAULT_TOKEN_CHANNEL", "HIDDEN"
	};

	public static String[] modeNames = {
		"DEFAULT_MODE"
	};

	private static String[] makeRuleNames() {
		return new String[] {
			"T__0", "T__1", "T__2", "DIGIT", "INT", "SUBREAL", "DOT", "REAL", "ID", 
			"WS", "UNCLOSE_STRING", "ILLEGAL_ESCAPE", "UNTERMINATE_COMMENT", "ERROR_CHAR"
		};
	}
	public static final String[] ruleNames = makeRuleNames();

	private static String[] makeLiteralNames() {
		return new String[] {
			null, "'Var'", "':'", "';'", null, null, "'.'"
		};
	}
	private static final String[] _LITERAL_NAMES = makeLiteralNames();
	private static String[] makeSymbolicNames() {
		return new String[] {
			null, null, null, null, "INT", "SUBREAL", "DOT", "REAL", "ID", "WS", 
			"UNCLOSE_STRING", "ILLEGAL_ESCAPE", "UNTERMINATE_COMMENT", "ERROR_CHAR"
		};
	}
	private static final String[] _SYMBOLIC_NAMES = makeSymbolicNames();
	public static final Vocabulary VOCABULARY = new VocabularyImpl(_LITERAL_NAMES, _SYMBOLIC_NAMES);

	/**
	 * @deprecated Use {@link #VOCABULARY} instead.
	 */
	@Deprecated
	public static final String[] tokenNames;
	static {
		tokenNames = new String[_SYMBOLIC_NAMES.length];
		for (int i = 0; i < tokenNames.length; i++) {
			tokenNames[i] = VOCABULARY.getLiteralName(i);
			if (tokenNames[i] == null) {
				tokenNames[i] = VOCABULARY.getSymbolicName(i);
			}

			if (tokenNames[i] == null) {
				tokenNames[i] = "<INVALID>";
			}
		}
	}

	@Override
	@Deprecated
	public String[] getTokenNames() {
		return tokenNames;
	}

	@Override

	public Vocabulary getVocabulary() {
		return VOCABULARY;
	}


	@Override
	public Token emit() {
	    Token result = super.emit();
	    switch (getType()) {
	    case UNCLOSE_STRING:       
	        throw new UncloseString(result.getText());
	    case UNTERMINATE_COMMENT:
	        throw new UnterminatedComment(result.getText());
	    case ILLEGAL_ESCAPE:
	    	throw new IllegalEscape(result.getText());
	    case ERROR_CHAR:
	    	throw new ErrorToken(result.getText());	
	    default:
	        return super.emit();
	    }
	}


	public BKITLexer(CharStream input) {
		super(input);
		_interp = new LexerATNSimulator(this,_ATN,_decisionToDFA,_sharedContextCache);
	}

	@Override
	public String getGrammarFileName() { return "BKIT.g4"; }

	@Override
	public String[] getRuleNames() { return ruleNames; }

	@Override
	public String getSerializedATN() { return _serializedATN; }

	@Override
	public String[] getChannelNames() { return channelNames; }

	@Override
	public String[] getModeNames() { return modeNames; }

	@Override
	public ATN getATN() { return _ATN; }

	public static final String _serializedATN =
		"\3\u608b\ua72a\u8133\ub9ed\u417c\u3be7\u7786\u5964\2\17V\b\1\4\2\t\2\4"+
		"\3\t\3\4\4\t\4\4\5\t\5\4\6\t\6\4\7\t\7\4\b\t\b\4\t\t\t\4\n\t\n\4\13\t"+
		"\13\4\f\t\f\4\r\t\r\4\16\t\16\4\17\t\17\3\2\3\2\3\2\3\2\3\3\3\3\3\4\3"+
		"\4\3\5\3\5\3\6\6\6+\n\6\r\6\16\6,\3\7\3\7\5\7\61\n\7\3\7\5\7\64\n\7\3"+
		"\7\7\7\67\n\7\f\7\16\7:\13\7\3\b\3\b\3\t\3\t\3\t\3\t\3\n\7\nC\n\n\f\n"+
		"\16\nF\13\n\3\13\6\13I\n\13\r\13\16\13J\3\13\3\13\3\f\3\f\3\r\3\r\3\16"+
		"\3\16\3\17\3\17\2\2\20\3\3\5\4\7\5\t\2\13\6\r\7\17\b\21\t\23\n\25\13\27"+
		"\f\31\r\33\16\35\17\3\2\5\3\2\62;\4\2--//\5\2\13\f\17\17\"\"\2Z\2\3\3"+
		"\2\2\2\2\5\3\2\2\2\2\7\3\2\2\2\2\13\3\2\2\2\2\r\3\2\2\2\2\17\3\2\2\2\2"+
		"\21\3\2\2\2\2\23\3\2\2\2\2\25\3\2\2\2\2\27\3\2\2\2\2\31\3\2\2\2\2\33\3"+
		"\2\2\2\2\35\3\2\2\2\3\37\3\2\2\2\5#\3\2\2\2\7%\3\2\2\2\t\'\3\2\2\2\13"+
		"*\3\2\2\2\r.\3\2\2\2\17;\3\2\2\2\21=\3\2\2\2\23D\3\2\2\2\25H\3\2\2\2\27"+
		"N\3\2\2\2\31P\3\2\2\2\33R\3\2\2\2\35T\3\2\2\2\37 \7X\2\2 !\7c\2\2!\"\7"+
		"t\2\2\"\4\3\2\2\2#$\7<\2\2$\6\3\2\2\2%&\7=\2\2&\b\3\2\2\2\'(\t\2\2\2("+
		"\n\3\2\2\2)+\5\t\5\2*)\3\2\2\2+,\3\2\2\2,*\3\2\2\2,-\3\2\2\2-\f\3\2\2"+
		"\2.\60\5\13\6\2/\61\7g\2\2\60/\3\2\2\2\60\61\3\2\2\2\61\63\3\2\2\2\62"+
		"\64\t\3\2\2\63\62\3\2\2\2\63\64\3\2\2\2\648\3\2\2\2\65\67\5\13\6\2\66"+
		"\65\3\2\2\2\67:\3\2\2\28\66\3\2\2\289\3\2\2\29\16\3\2\2\2:8\3\2\2\2;<"+
		"\7\60\2\2<\20\3\2\2\2=>\5\r\7\2>?\5\17\b\2?@\5\r\7\2@\22\3\2\2\2AC\5\21"+
		"\t\2BA\3\2\2\2CF\3\2\2\2DB\3\2\2\2DE\3\2\2\2E\24\3\2\2\2FD\3\2\2\2GI\t"+
		"\4\2\2HG\3\2\2\2IJ\3\2\2\2JH\3\2\2\2JK\3\2\2\2KL\3\2\2\2LM\b\13\2\2M\26"+
		"\3\2\2\2NO\13\2\2\2O\30\3\2\2\2PQ\13\2\2\2Q\32\3\2\2\2RS\13\2\2\2S\34"+
		"\3\2\2\2TU\13\2\2\2U\36\3\2\2\2\t\2,\60\638DJ\3\b\2\2";
	public static final ATN _ATN =
		new ATNDeserializer().deserialize(_serializedATN.toCharArray());
	static {
		_decisionToDFA = new DFA[_ATN.getNumberOfDecisions()];
		for (int i = 0; i < _ATN.getNumberOfDecisions(); i++) {
			_decisionToDFA[i] = new DFA(_ATN.getDecisionState(i), i);
		}
	}
}