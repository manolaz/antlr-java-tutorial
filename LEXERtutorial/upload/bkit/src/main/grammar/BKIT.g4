/**
 * Student name:
 * Student ID:
 */
grammar BKIT;

@lexer::header{
	package main.bkit.parser;
}

@lexer::members{
@Override
public Token emit() {
    Token result = super.emit();
    switch (getType()) {
    case UNCLOSE_STRING:       
        throw new UncloseString(result.getText());
    case UNTERMINATE_COMMENT:
        throw new UnterminatedComment(result.getText());
    case ILLEGAL_ESCAPE:
    	throw new IllegalEscape(result.getText());
    case ERROR_CHAR:
    	throw new ErrorToken(result.getText());	
    default:
        return super.emit();
    }
}
}

@parser::header{
	package main.bkit.parser;
}

options{
	language=Java;
}

program  : 'Var' ':' ID ';' EOF ; 


// QUESTION2

// fragment DIGIT : [0-9] ;
// INT : DIGIT+ ;
// fragment Letter: [a-z];
// Manyletter:  Letter+ ;
// ID : (Letter)+((Manyletter)|(INT))*;

// QUESTION 3
// a) For a number to be taken as "real" (or "floating point") format, 
// it must either have a
// decimal point, or use scientific notation.
//  For example, 1.0, 1e-12, 1.0e-12, 0.000000001
// are all valid reals.
//  At least one digit must exist on either side of a decimal point.
// b) Strings are made up of a sequence of characters between single quotes: ’string’.
//  The single quote itself can appear as two single quotes
//  back to back in a string: ’isn”t’.
fragment DIGIT : [0-9] ;
INT : DIGIT+ ;
SUBREAL : (INT)('e')?('+'|'-')?(INT)*;
DOT: '.';
REAL : SUBREAL DOT SUBREAL;
ID : (REAL)*;

// Question 4.
// Find regular expressions and state diagrams of the equivalent
//  NFA for each of the following
// descriptions.
// a) {a
// n
// b
// m|n ≥ 0, m > 2}
// b) {a
// n
// b
// m|n + m > 0, n + m is even}
// c) {a
// n
// b|n mod 3 = 1}

WS : [ \t\r\n]+ -> skip ; // skip spaces, tabs, newlines

UNCLOSE_STRING: .;
ILLEGAL_ESCAPE: .;
UNTERMINATE_COMMENT: .;
ERROR_CHAR: .;

