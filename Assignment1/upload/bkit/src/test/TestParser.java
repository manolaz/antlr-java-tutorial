

/**
 * @author nhphung
 */
package test;

import java.io.*;
//import java.util.*;
import org.antlr.v4.runtime.ANTLRFileStream;
import org.antlr.v4.runtime.CommonTokenStream;
import org.antlr.v4.runtime.BaseErrorListener;
import main.bkit.parser.*;
import org.junit.Test;
import static org.junit.Assert.assertEquals;



//import ProgramContext;

public class TestParser {
  public static String inputdir = ".";
  public static String outputdir = ".";
  private static final String sepa = java.io.File.separator;

  private String check(String input, int num) 
     {
      String infile, outfile;
      ANTLRFileStream instream;
      PrintWriter outstream;
      String res = "";
      infile = TestParser.inputdir + sepa + num  + ".txt";
      try {
        outstream = new PrintWriter(infile);
        outstream.println(input);
        outstream.close();
        instream = new ANTLRFileStream(infile);

        outfile = TestParser.outputdir + sepa + num  + ".txt";
        outstream = new PrintWriter(new File(outfile));
        test(instream,outstream);
        outstream.close();

        BufferedReader in = new BufferedReader(new FileReader(outfile));
        StringBuilder sb = new StringBuilder();
        String line = in.readLine();
        while(line != null) {
          sb.append(line);
          line = in.readLine();
        }
        in.close();
        res = sb.toString();   
        //System.out.println(res);
      }
      catch (IOException  e) {
        System.out.println("Folder "+inputdir + " or "+ outputdir + " not found");
        System.exit(0);
      }
      return res;
  }

    /**
     * Parses an expression into an integer result.
     * @param expression the expression to part
     * @return and integer result
     */
    private void test(ANTLRFileStream fileName,PrintWriter outFile) {
//    def test(fileName:ANTLRFileStream,outFile:PrintWriter) = {

        BKITLexer lexer;
        lexer = new BKITLexer(fileName); // ANTLRInputStream
        BaseErrorListener _listener = ProcessError.createErrorListener();
        CommonTokenStream tokens = new CommonTokenStream(lexer);

        BKITParser parser = new BKITParser(tokens);
        parser.removeErrorListeners();
        parser.addErrorListener(_listener);
        try {
          parser.program();
          outFile.print("sucessful");
        } catch (Exception e){
          outFile.print(e.getMessage());
        }
    
    }

    @Test
    public void testSimpleProgramWithVar() {
      String input = "Var: abc;";
      String output = "sucessful";
      assertEquals(output,check(input,1));
    }
    @Test
    public void testSimpleProgramWithFunction() {
      String input = "Function: abc \nBody:\nEndBody.";
      String output = "sucessful";
      assertEquals(output,check(input,2));
    }
    @Test
    public void testErrorProgram() {
      String input = "Var abc;";
      String output = "Error on line 1 col 5";
      assertEquals(output,check(input,3));
    }
}