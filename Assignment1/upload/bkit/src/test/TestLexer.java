

/**
 * @author nhphung
 */
package test;
import org.antlr.v4.runtime.Token;
import java.io.*;
//import java.util.*;
import org.antlr.v4.runtime.ANTLRFileStream;
import main.bkit.parser.*;
import org.junit.Test;
import static org.junit.Assert.assertEquals;

public class TestLexer {
  public static String inputdir = ".";
  public static String outputdir = ".";
  private static final String sepa = java.io.File.separator;
  @Test
   public void testIdentifier1() {
      String input = "abc";
      String output = "abc,<EOF>";
      assertEquals(output,check(input,1));
   }
   @Test 
   public void testErrorToken() {
      String input = "Var : abc ? def";
      String output = "Var,:,abc,Error Token: ?";
      assertEquals(output,check(input,2));
   }
   @Test
   public void testUnterminatedComment() {
    String input = "abc ** def";
    String output = "abc,Unterminated Comment: ** def";
    assertEquals(output,check(input,3));
   }

/*  public static void main(String args[]) 
  throws IOException,FileNotFoundException {
    String file,out;
    ANTLRFileStream infile;
    PrintWriter outFile;
    file = (args.length > 0)? args[0] : "test.txt";
    infile = new ANTLRFileStream(file);
    out = (args.length > 1)? args[1] : "output.txt"; 
    outFile = new PrintWriter(new File(out));
    test(infile,outFile);
    outFile.close();
  }*/
  private String check(String input, int num)  {
      String infile, outfile;
      ANTLRFileStream instream;
      PrintWriter outstream;
      String res = "";
      infile = TestLexer.inputdir + sepa + num  + ".txt";
      try {
        outstream = new PrintWriter(infile);
        outstream.println(input);
        outstream.close();
        instream = new ANTLRFileStream(infile);

        outfile = TestLexer.outputdir + sepa + num  + ".txt";
        outstream = new PrintWriter(new File(outfile));
        test(instream,outstream);
        outstream.close();

        BufferedReader in = new BufferedReader(new FileReader(outfile));
        StringBuilder sb = new StringBuilder();
        String line = in.readLine();
        while(line != null) {
          sb.append(line);
          line = in.readLine();
        }
        in.close();
        res = sb.toString();   
      }
      catch (IOException  e) {
        System.out.println("Folder "+inputdir + " or "+ outputdir + " not found");
        System.exit(0);
      }
      return res;
  }
  private void test(ANTLRFileStream infile,PrintWriter outfile) {
    BKITLexer lexer;
    lexer = new BKITLexer(infile);
      
      try {
        printLexeme(lexer,outfile);
      }
      catch (ErrorToken et){
        outfile.println("ErrorToken "+et.s);
      }
      catch (IllegalEscape ie){
        outfile.println("Illegal escape in string: "+ie.s);   
      }
      catch (UncloseString us) {
        outfile.println("Unclosed string: "+ us.s);
      }
      catch (UnterminatedComment uc) {
        outfile.println("Unterminated Comment: "+ uc.s);
      }
  }
  private void printLexeme(BKITLexer lexer,PrintWriter dev){
    do {
      Token tok = lexer.nextToken();
      if (tok.getType() != Token.EOF) {
        dev.print(tok.getText()+",");
      } else {
        dev.print(tok.getText());
        break;
      }
    } while (true);   
  }
}