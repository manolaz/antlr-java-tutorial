package test;
import org.junit.runner.JUnitCore;
import org.junit.runner.Result;
import org.junit.runner.notification.Failure;

public class TestRunner {
   public static void main(String[] args) {
   		String option = "",indir = "",outdir ="";
	   	if (args.length == 3) {
	      option = args[0].substring(1);
	      indir = args[1];
      	  outdir = args[2];
	    }
	    else {
	    	System.out.println("Usage: java test.TestRunner -phase1 indir outdir\n" 
	    			+ "or\n"
	    			+ "java test.TestRunner -phase2 indir outdir\n");
	    	System.exit(0);
	    }
	    Result result;
	    if (option.equals("phase1")) {
	    	TestLexer.inputdir = indir;
	    	TestLexer.outputdir = outdir;
	     	result = JUnitCore.runClasses(TestLexer.class);
	    }
	    else {
	    	TestParser.inputdir = indir;
	    	TestParser.outputdir = outdir;
			result = JUnitCore.runClasses(TestParser.class);
	    }
	    for (Failure failure : result.getFailures()) {
	        System.out.println(failure.toString());
	    }
			
	    System.out.println(result.wasSuccessful());
	}
}  