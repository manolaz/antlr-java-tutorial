/**
 * Student name:
 * Student ID:
 */
grammar BKIT;

@lexer::header{
	package main.bkit.parser;
}

@lexer::members{
@Override
public Token emit() {
    Token result = super.emit();
    switch (getType()) {
    case UNCLOSE_STRING:       
        throw new UncloseString(result.getText());
    case UNTERMINATE_COMMENT:
        throw new UnterminatedComment(result.getText());
    case ILLEGAL_ESCAPE:
    	throw new IllegalEscape(result.getText());
    case ERROR_CHAR:
    	throw new ErrorToken(result.getText());	
    default:
        return super.emit();
    }
}
}

@parser::header{
	package main.bkit.parser;
}

options{
	language=Java;
}

program  : 'Var' ':' ID ';' EOF ; 

ID : [a-z]+ ;             // match lower-case identifiers

WS : [ \t\r\n]+ -> skip ; // skip spaces, tabs, newlines

UNCLOSE_STRING: .;
ILLEGAL_ESCAPE: .;
UNTERMINATE_COMMENT: .;
ERROR_CHAR: .;

