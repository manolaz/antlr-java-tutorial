1. Java version 7 or above must be installed
2. Setup junit (read https://www.tutorialspoint.com/junit/junit_environment_setup.htm)
3. Download antlr-4.8-complete.jar from antlr.org/download.html section ANTLR Tool and Java Target
4. Set the environment variable ANTLR to the downloaded file antlr-4.8-complete.jar
For Linux/MacOS:
5. Open Terminal
6. Change folder to the upload/linux folder 
7. Type ./gen.sh (no error message)
8. Type ./comp.sh (0 or 2 warning messages)
9. Type ./lextest.sh (2 error messages)
10.Type ./regtest.sh (1 error message)
For Windows: (I have not tried this version as I don't have Windows machine, I will fix the following scripts in class)
5. Open Command Prompt
6. Change folder to the upload/windows folder 
7. Type gen.bat (no error message)
8. Type comp.bat (0 or 2 warning messages)
9. Type lextest.bat (2 error messages)
10.Type regtest.bat (1 error message)
